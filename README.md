# workflows-examples

## Getting started

You first need to create your workflow in Google Cloud Workflows

You can use gcloud build or simply create a trigger on this folder

## Run a cloud run job from Google Cloud Workflows

For instance you can create (if it does not exist) and run a job with this config to google cloud workflows

```
{
    "job_name": "xml-loader-small",
    "cpu": "1000m",
    "memory": "512Mi",
    "container_registery_path": "europe-docker.pkg.dev/retail-cpg-luxury/jobs",
    "container_name":"xml_to_bq_loader:latest",
    "container_cmd": ["python3","/xml_to_bq.py"],
    "container_env":[
        { "name": "PROJECT_ID","value": "retail-cpg-luxury"},
        { "name": "GCS_XML_PATH","value": "gs://retailai/cu_catalogues/exportPriceBook_uexpress.xml"},
        { "name": "XML_XPATH","value": ".//doc:price-table"},
        { "name": "XML_NAMESPACES","value": "http://www.demandware.com/xml/impex/pricebook/2006-10-31"},
        { "name": "TABLE_ID","value": "retail_demo.xml_load2"}
    ]
}
```